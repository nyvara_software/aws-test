AWS.config.region = 'us-west-2';
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-west-2:fafb9a05-b4a8-4cc7-ac9a-8cbbe4a8d3e7',
    Logins: {
      'cognito-idp.us-west-2.amazonaws.com/us-west-2_62idqE7Mc': JSON.parse(localStorage.getItem('token'))
    }
});

$(document).ready(function(){
  updateAuthenticationStatus();
  loadWeather();
});
function logout(){
  localStorage.clear();
  window.location = '/';
};

function updateAuthenticationStatus(){
  $('#user').empty();
  $('#login').empty();
  var user = localStorage.getItem('token');
  if(user){
    $('#user').show().append('<a onclick="logout()">Log out</a>');
    $('#login').hide();
  } else {
    $('#login').show().append('<a href="/login">Log in</a>');
    $('#user').hide();
  }
}

function loadWeather(){
  if(window.location.pathname == '/weather/'){
    if(localStorage.getItem('token')){
      AWS.config.credentials.get(function (err) {
        var client = apigClientFactory.newClient({
          accessKey: AWS.config.credentials.accessKeyId, 
          secretKey: AWS.config.credentials.secretAccessKey, 
          sessionToken: AWS.config.credentials.sessionToken,
          region: 'us-west-2'  
        });
        client.rootGet().then(function(data){
          console.log(data);
            $('#forecast').append('<h4>' + data.data.City + ' Temperature:' + data.data.Temperature + 'C </h4>');
        });
      });
    } else {
      window.location = '/';
    }
  }
}

$('#signin').submit(function(e){
  e.preventDefault();
  AWSCognito.config.region = 'us-west-2';
  AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-west-2:fafb9a05-b4a8-4cc7-ac9a-8cbbe4a8d3e7'
  });
  // Need to provide placeholder keys unless unauthorised user access is enabled for user pool
  AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'});

  var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool({ 
    UserPoolId : 'us-west-2_62idqE7Mc',
    ClientId : '2gg4fi7iprk2iik9c5imj80863'
  });

  var authenticationData = {
    Username : $('#username').val(),
    Password : $('#password').val(),
  };
  var userData = {
    Username : $('#username').val(),
    Pool : userPool
  };
  var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
  var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

  cognitoUser.authenticateUser(authenticationDetails, {
    onSuccess: function (result) {
      localStorage.setItem('token', JSON.stringify(result.idToken.jwtToken));
      window.location = '/';
    },
    onFailure: function(err) {
      console.log(err);
    }
  });
})