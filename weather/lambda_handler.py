import boto3
import dynamoutils
import json
from flask import Flask
from botocore.exceptions import ClientError

app = Flask(__name__)

@app.route('/', methods=["GET"])
def index():
    city_weather = get_city_weather_forecast()
    response = json.dumps(city_weather, indent = 4, cls=dynamoutils.DecimalEncoder)
    return response

@app.route('/city/<city>', methods=["GET"])
def weather_by_city(city):
    city_weather = get_city_weather_forecast(city)
    response = json.dumps(city_weather, indent = 4, cls=dynamoutils.DecimalEncoder)
    return response

@app.route('/city/<city>/day/<day>', methods=["GET"])
def weather_by_city_and_day(city, day):
    city_weather = get_city_weather_forecast(city, day)
    response = json.dumps(city_weather, indent = 4, cls=dynamoutils.DecimalEncoder)
    return response

def get_city_weather_forecast(city = "Toronto", daysOffset = 1):
    table = get_weather_table()
    try:
        response = table.get_item(
            Key={
                'City': city,
                'DaysOffset': daysOffset
            }
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        item = response['Item']
        return item

def get_weather_table():
    dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="https://dynamodb.us-west-2.amazonaws.com")
    table = dynamodb.Table('Weather')
    return table

@app.after_request
def add_cors(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

if __name__ == "__main__":
    app.run()